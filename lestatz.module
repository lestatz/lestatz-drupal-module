<?php

/**
 * @file
 * Manage and use LeStatz.
 */

define('LESTATZ_SERVER', '//lestatz.e2go.org/');
define('LESTATZ_PUBLIC_JS', '//lestatz.e2go.org/public/publicLeStatz.js');

/**
 * Implements hook_menu().
 */
function lestatz_menu() {

	// This should not even exist

}

/**
 * Implements hook_permission().
 */
function lestatz_permission() {
  return array(
    'skip lestatz' => array(
      'title' => t('Skip getting registered in LeStatz'),
      'description' => t('This group will not be taken into account for web analytics.'),
    ),
    'admin lestatz' => array(
      'title' => t('Administer LeStatz settings'),
      'description' => t('Users who can administer LeStatz'),
    ),
  );
}

/**
 * Settings page.
 */
function lestatz_settings_form($form, &$form_state) {

  global $base_url;

  $form['clientToken'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get a free client Token'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['clientToken']['info'] = array(
    '#type' => 'item',
    '#markup' => t('Get a free client token and use our service, more details !here.', array('!here' => l(t('at irl.e2go.org'), "https://irl.e2go.org"))),
  );

  $form['clientToken']['get_token'] = array(
    '#type' => 'button',
    '#value' => t('Get a free client token'),
    '#attributes' => array('onClick' => 'irl_client_register_new()'),
    '#executes_submit_callback' => FALSE,
  );

  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('irl server settings'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['server']['info'] = array(
    '#type' => 'item',
    '#markup' => t('You may use your own irl server or request client Id at any time, it is free. You can use !thisForm.', array('!thisForm' => l(t('this form'), "irl/client_register"))),
  );

  $form['server']['irl_server_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the irl server.'),
    '#default_value' => variable_get('irl_server_path', LESTATZ_SERVER),
    '#size' => 25,
    '#maxlength' => 50,
    '#description' => t('Location of your "irl" server. Best if you use "https". You can always use our <a href="@url">free server</a>. You will need an account.', array('@url' => 'https://e2go.lasangha.org')),
    '#required' => TRUE,
  );

  $form['server']['irl_server_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret token.'),
    '#default_value' => variable_get('irl_server_token', '0'),
    '#size' => 25,
    '#maxlength' => 150,
    '#description' => t('The secret token (set by you) in YOUR OWN irl server. If you are using e2go as a provider then this is your client Id.'),
    '#required' => TRUE,
  );

  $form['server']['irl_server_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your client Id.'),
    '#default_value' => variable_get('irl_server_client_id', '1'),
    '#size' => 25,
    '#maxlength' => 150,
    '#description' => t('A unique Id for this "client", given to you by your provider. If you are hosting your own server you can use the default value. If you are connecting more than one client to the same server use different ids for each one. If you are using e2go as a provider, this is your email address.'),
    '#required' => TRUE,
  );

  $form['reg'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration settings'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['reg']['irl_allow_registration'] = array(
    '#type' => 'select',
    '#title' => t('Allow new users to register via irl. Very experimental! Not recomended for production sites.'),
    '#default_value' => variable_get('irl_allow_registration', '0'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t('New users will be able to register via irl.'),
    '#required' => TRUE,
  );

  $form['reg']['irl_require_email_for_registration'] = array(
    '#type' => 'select',
    '#title' => t('Allow new users to register via irl WITHOUT an email.'),
    '#default_value' => variable_get('irl_require_email_for_registration', '1'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t('New users may register without a valid email address.'),
    '#required' => TRUE,
  );

  $form['reg']['irl_allow_global_offline_solve'] = array(
    '#type' => 'select',
    '#title' => t('Allow new users to solve challenges while offline.'),
    '#default_value' => variable_get('irl_allow_global_offline_solve', '1'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t('New will have the option to type in the challenge solution in your website. Useful if they have no internet on their phones or tablets.'),
    '#required' => TRUE,
  );

  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['misc']['irl_challenge_max_time'] = array(
    '#type' => 'select',
    '#title' => t("User's maximum time to solve a challenge. In seconds."),
    '#default_value' => variable_get('irl_challenge_max_time', 60),
    '#options' => array(
      60 => 60,
      45 => 45,
      30 => 30,
      15 => 15,
      10 => 10,
    ),
    '#description' => t('After this time the user will be redirected to the front page.'),
    '#required' => TRUE,
  );

  $form['2fa'] = array(
    '#type' => 'fieldset',
    '#title' => t('Two Factor Authentication'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['2fa']['irl_2fa_allow_users'] = array(
    '#type' => 'select',
    '#title' => t('Allow new users to use irl as 2fa'),
    '#default_value' => variable_get('irl_2fa_allow_users', '0'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t('Users may secure their account with a 2fa system.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_block_info().
 */
function lestatz_block_info() {

	$blocks['lestatz_script'] = array(
	  'info' => t('LeStatz web analytics'),
	  'cache' => DRUPAL_NO_CACHE
  );

	return $blocks;

}

/**
 * Implements hook_block_view
 */
function lestatz_block_view($delta = '') {

	switch ($delta) {
	case 'lestatz_script':

		$block['subject'] = t('');
		$block['content'] = _lestatz_block_analytics_view();

		return $block;
	}
}

/**
 * Implements hook_block_configure.
 */
function lestatz_block_configure($delta = '') {

	$form = array();

	if ($delta == 'lestatz_script') {

		$form['conf'] = array(
			'#type' => 'fieldset',
			'#title' => t('LeStatz settings'),
			'#weight' => 0,
			'#collapsible' => FALSE,
			'#collapsed' => FALSE,
		);

		$form['conf']['info'] = array(
			'#type' => 'item',
			'#markup' => t('You may register additional parameter about the page displayed and use it to create special filters and permissions for administrators. For example you could have a special filter just for pages in Spanish, another one for "Blog posts in French" and many more. "Goals" and "Ref" are always included, this is automatic.'),
		);

		$form['conf']['lestatz_js'] = array(
			'#type' => 'textfield',
			'#title' => t('LeStatz javascript file)'),
			'#default_value' => variable_get('lestatz_js', LESTATZ_PUBLIC_JS),
			'#size' => 60,
			'#description' => t('The js file that you will use, this can be different from the server. Default is !jsFile', array('!jsFile' => LESTATZ_PUBLIC_JS)),
			'#required' => TRUE,
		);

		$form['conf']['lestatz_server_url'] = array(
			'#type' => 'textfield',
			'#title' => t('LeStatz server url (use trailing /)'),
			'#default_value' => variable_get('lestatz_server_url', LESTATZ_SERVER),
			'#size' => 60,
			'#description' => t('If you user e2go you will needa free token, get it with the free App. Or you can set up your own server. Default is !jsServer', array('!jsServer' => LESTATZ_SERVER)),
			'#required' => TRUE,
		);

		$form['conf']['lestatz_client_token'] = array(
			'#type' => 'textfield',
			'#title' => t('Client token'),
			'#default_value' => variable_get('lestatz_client_token', '000'),
			'#size' => 60,
			'#description' => t('Your client token, only required if you are using e2go. If you host your own server use anything you want.'),
			'#required' => TRUE,
		);

		$form['conf']['lestatz_site_token'] = array(
			'#type' => 'textfield',
			'#title' => t('Site token'),
			'#default_value' => variable_get('lestatz_site_token', '0'),
			'#size' => 60,
			'#description' => t('Get this from the App in the site`s admin area.'),
			'#required' => TRUE,
		);

		$form['conf']['lestatz_register_groups'] = array(
			'#type' => 'select',
			'#title' => t('Register the groups of the owner of the entity (node)'),
			'#default_value' => variable_get('lestatz_register_groups', 1),
			'#options' => array(
				'1' => 'Yes',
				'0' => 'No'
			),
		);

		$form['conf']['lestatz_register_user'] = array(
			'#type' => 'select',
			'#title' => t('Register the active user'),
			'#default_value' => variable_get('lestatz_register_user', 1),
			'#options' => array(
				'1' => 'Yes',
				'0' => 'No'
			),
			'#description' => t('Register the uid of the user viewing the page.'),
		);

		$form['conf']['lestatz_register_node'] = array(
			'#type' => 'select',
			'#title' => t('Register the details about the entity (node)'),
			'#default_value' => variable_get('lestatz_register_node', 1),
			'#options' => array(
				'1' => 'Yes',
				'0' => 'No'
			),
			'#description' => t('This is the node id, user id (owner of the node) and type.'),
		);

		$form['conf']['lestatz_register_lang'] = array(
			'#type' => 'select',
			'#title' => t('Register the languge in which the page is displayed'),
			'#default_value' => variable_get('lestatz_register_lang', 1),
			'#options' => array(
				'1' => 'Yes',
				'0' => 'No'
			)
		);

	}

	return $form;

}

/**
 * Implements hook_block_save.
 */
function lestatz_block_save($delta = '', $edit = array()) {

  if ($delta == 'lestatz_script') {
    variable_set('lestatz_server_url', $edit['lestatz_server_url']);
    variable_set('lestatz_js', $edit['lestatz_js']);
    variable_set('lestatz_client_token', $edit['lestatz_client_token']);
    variable_set('lestatz_site_token', $edit['lestatz_site_token']);
    variable_set('lestatz_register_lang', $edit['lestatz_register_lang']);
    variable_set('lestatz_register_groups', $edit['lestatz_register_groups']);
    variable_set('lestatz_register_node', $edit['lestatz_register_node']);
    variable_set('lestatz_register_user', $edit['lestatz_register_user']);
  }
}

/**
 * Helper function to generate the contents of the analytics block.
 */
function _lestatz_block_analytics_view(){

	global $user, $language;

	$extras = [];

	if(variable_get('lestatz_register_groups', 1) == 1) {
		$extras['userRoles'] = $user->roles;
	}

	if(variable_get('lestatz_register_lang', 1) == 1) {
		$extras['lango'] = $language->name;
	}

	if(variable_get('lestatz_register_node', 1) == 1) {
		$n = node_load(arg(1));
		if($n){
			$extras['node'] = ['nid' => $n->nid, 'uid' => $n->uid, 'type' => $n->type];
		}
	}

	if(variable_get('lestatz_register_user', 1) == 1) {
		$extras['uid'] = $user->uid;
	}

	$settings = "// LeStatz_ini;\n";
	$settings .= sprintf("LeStatz_uid = '%s'; \n", variable_get('lestatz_client_token', '000'));
	$settings .= sprintf("LeStatz_domain = '%s'; \n", variable_get('lestatz_site_token', '000'));
	$settings .= sprintf("LeStatz_extras = '%s';", JSON_encode($extras));
	$settings .= sprintf("LeStatz_server = '%s';", variable_get('lestatz_server_url', LESTATZ_SERVER));
	//$settings .= 'LeStatz_divId = "LeStatz"';
	$settings .= '// LeStatz_end';

	drupal_add_js($settings, 'inline');
	drupal_add_js(variable_get('lestatz_js', LESTATZ_PUBLIC_JS), 'external');

	return '';
}

